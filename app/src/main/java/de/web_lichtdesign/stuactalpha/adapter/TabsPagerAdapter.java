package de.web_lichtdesign.stuactalpha.adapter;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import de.web_lichtdesign.stuactalpha.tabs.ScoreFragment;

/**
 * Created by moritz on 13.06.17.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity

            case 1:
                // Games fragment activity
               // return new GamesFragment();
            case 2:
                return new ScoreFragment();
                // Movies fragment activity
                //return new MoviesFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
}
