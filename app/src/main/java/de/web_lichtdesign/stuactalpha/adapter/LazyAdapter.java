package de.web_lichtdesign.stuactalpha.adapter;

/**
 * Created by moritz on 15.06.17.
 */

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.helper.ImageLoader;
import de.web_lichtdesign.stuactalpha.R;

public class LazyAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String,String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader;

    public LazyAdapter(Activity a, ArrayList<HashMap<String,String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.news_feed_item, null);

        TextView text=(TextView)vi.findViewById(R.id.txtTitle);;
        TextView comment=(TextView)vi.findViewById(R.id.txtComment);;
        TextView username=(TextView)vi.findViewById(R.id.txtUsername);;
        ImageView image=(ImageView)vi.findViewById(R.id.imgThumbnail);
        text.setText(data.get(position).get("title"));
        comment.setText(data.get(position).get("comment"));
        username.setText(data.get(position).get("username"));
        String fileurl = AppConfig.URL_GET_IMAGE+data.get(position).get("file1");

        if(!data.get(position).get("file1").equals("null")){
            Log.e("Fileurl", fileurl);
            imageLoader.DisplayImage(fileurl, image);

        }

        return vi;
    }
}