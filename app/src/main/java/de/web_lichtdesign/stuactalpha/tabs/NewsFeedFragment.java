package de.web_lichtdesign.stuactalpha.tabs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.activity.MainActivity;
import de.web_lichtdesign.stuactalpha.adapter.LazyAdapter;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;
import de.web_lichtdesign.stuactalpha.activity.ShowActivity;



public class NewsFeedFragment extends Fragment{
    private ProgressDialog pDialog;
    private ListView lv;
    private ArrayList<HashMap<String, String>> newsFeedList;
    private ListAdapter adapter;

    public NewsFeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        newsFeedList = new ArrayList<>();
        adapter = new LazyAdapter(getActivity(), newsFeedList);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);
        lv = (ListView) view.findViewById(R.id.lstNewsFeed);
        getNewsFeed();


        return view;

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getNewsFeed();

        }
    }

    private Void getNewsFeed(Void... arg0) {
        String tag_string_req = "req_list_activities";


        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_NEWSFEED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                newsFeedList.clear();


                try {

                    JSONArray activities = new JSONArray(response);


                    for (int i = 0; i < activities.length(); i++) {
                        JSONObject c = activities.getJSONObject(i);
                        HashMap<String, String> tmp = new HashMap<>();



                        //tmp.put("title",c.getJSONObject("activity").getString("title"));
                        tmp.put("username",c.getJSONObject("user").getString("username"));
                        String a = "activity";
                        tmp.put("file1", c.getString("file1"));
                        //tmp.put("title",c.getJSONObject("activity").getString("title"));
                        tmp.put("comment", c.getString("com"));
                        tmp.put("username",c.getJSONObject(a).getString("title"));





                        // tmp hash map for single contact


                        // adding each child node to HashMap key => value






                        // adding contact to contact list
                        newsFeedList.add(tmp);
                    }
                    if (pDialog.isShowing())
                        pDialog.dismiss();

                    adapter = new LazyAdapter(getActivity(), newsFeedList);
                    lv.setAdapter(adapter);





                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().getRequestQueue().getCache().remove(AppConfig.URL_NEWSFEED);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

        return null;


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}