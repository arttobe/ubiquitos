package de.web_lichtdesign.stuactalpha.tabs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.activity.MainActivity;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;
import de.web_lichtdesign.stuactalpha.activity.ShowActivity;



public class ActivityListFragment extends Fragment{
    private ProgressDialog pDialog;
    private ListView lv;
    private ArrayList<HashMap<String, String>> activityList;
    private ListAdapter adapter;

    public ActivityListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        activityList = new ArrayList<>();

        /*adapter = new SimpleAdapter(
                getContext(), activityList,
                R.layout.list_activity_item, new String[]{"title", "typ", "points","id"
        }, new int[]{R.id.a_title,R.id.a_typ, R.id.a_points});*/




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

            View view = inflater.inflate(R.layout.fragment_activity_list, container, false);
            lv = (ListView) view.findViewById(R.id.list_activity);

            getActivities();



            return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getActivities();

        }
    }

    private Void getActivities(Void... arg0) {
        String tag_string_req = "req_list_activities";
        pDialog.setMessage("Loading");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_LIST_ACTIVITIES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                activityList.clear();
                Log.e("ActivityList", String.valueOf(activityList.size()));



                try {

                    JSONArray activities = new JSONArray(response);

                    for (int i = 0; i < activities.length(); i++) {
                        HashMap<String, String> tmp = new HashMap<>();

                        JSONObject c = activities.getJSONObject(i);

                        String id = c.getString("id");
                        String title = c.getString("title");
                        String points = "Points: "+c.getString("points");
                        String typ = "Category: "+ c.getJSONObject("activity_category").getString("title");
                        Log.e("title", title);


                        // tmp hash map for single contact


                        // adding each child node to HashMap key => value
                        tmp.put("id", id);
                        tmp.put("title", title);
                        tmp.put("typ", typ);
                        tmp.put("points", points);



                        // adding contact to contact list
                        activityList.add(tmp);
                    }


                    Log.e("ActivityList", String.valueOf(activityList.size()));



                    adapter = new SimpleAdapter(
                            getContext(), activityList,
                            R.layout.list_activity_item, new String[]{"title", "typ", "points","id"
                    }, new int[]{R.id.a_title,R.id.a_typ, R.id.a_points});


                    lv.setAdapter(adapter);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent intent = new Intent(getContext(),ShowActivity.class);
                            intent.putExtra("id", activityList.get(position).get("id"));
                            //based on item add info to intent
                            startActivity(intent);

                        }
                    });
                    if (pDialog.isShowing())
                        pDialog.dismiss();





                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().getRequestQueue().getCache().remove(AppConfig.URL_LIST_ACTIVITIES);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        hideDialog();


        return null;


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}