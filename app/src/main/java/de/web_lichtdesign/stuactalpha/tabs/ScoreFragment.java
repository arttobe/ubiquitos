package de.web_lichtdesign.stuactalpha.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;
import de.web_lichtdesign.stuactalpha.helper.SQLiteHandler;
import de.web_lichtdesign.stuactalpha.helper.SessionManager;


public class ScoreFragment extends Fragment{
    TextView score;
    HashMap<String,String> data;
    HashMap<String, String> user;
    private SQLiteHandler db;
    private SessionManager session;

    public ScoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        db = new SQLiteHandler(getContext());

        // session manager
        session = new SessionManager(getContext());



        // Fetching user details from sqlite
        user = db.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_score, container, false);
        score = (TextView) view.findViewById(R.id.txtScore);
        getScore(user.get("id"));
        return view;
    }

    private Void getScore(final String id) {
        String tag_string_req = "req_show_activity";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SCORE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                score.setText("Dein Score: " + response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


        return null;


    }

}