package de.web_lichtdesign.stuactalpha.app;

/**
 * Created by moritz on 11.06.17.
 */

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://stuact.lichtdesign-rinckens.de/users/login";
    public static String URL_SCORE = "http://stuact.lichtdesign-rinckens.de/users/score";

    // Server user register url
    public static String URL_REGISTER = "http://stuact.lichtdesign-rinckens.de/users/register";
    public static String URL_LIST_ACTIVITIES = "http://stuact.lichtdesign-rinckens.de/activities/list-all";
    public static String URL_SHOW_ACTIVITY = "http://stuact.lichtdesign-rinckens.de/activities/show";
    public static String URL_DO_ACTIVITY = "http://stuact.lichtdesign-rinckens.de/activities-users/add";
    public static String URL_NEWSFEED = "http://stuact.lichtdesign-rinckens.de/activities-users/feed";
    public static String URL_GET_IMAGE = "http://stuact.lichtdesign-rinckens.de/files/ActivitiesUsers/file1/";
}
