package de.web_lichtdesign.stuactalpha.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.web_lichtdesign.stuactalpha.R;


import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;

public class ShowActivity extends AppCompatActivity {
    private TextView title;
    private TextView points;
    String type;
    private Toolbar toolbar;
    private HashMap<String,String> dataMap;

    private TextView description;
    private ProgressDialog pDialog;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i= getIntent();
        id= i.getStringExtra("id");
        title = (TextView) findViewById(R.id.title);
        points = (TextView) findViewById(R.id.points);

        description = (TextView) findViewById(R.id.description);
        dataMap = new HashMap<String, String>();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        getActivity(id);




    }

    private Void getActivity(final String id) {
        String tag_string_req = "req_show_activity";
        pDialog.setMessage("Loading");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SHOW_ACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                String t = "";

                try {

                    JSONObject activity = new JSONObject(response);
                    dataMap.put("title", activity.getString("title"));
                    dataMap.put("id", activity.getString("id"));
                    dataMap.put("points", activity.getString("points"));
                    dataMap.put("value1", activity.getString("value1"));
                    dataMap.put("value2", activity.getString("value2"));
                    dataMap.put("value3", activity.getString("value3"));
                    dataMap.put("value4", activity.getString("value4"));
                    dataMap.put("description", activity.getString("description"));
                    dataMap.put("category", activity.getJSONObject("activity_category").getString("title"));
                    dataMap.put("type", activity.getJSONObject("activity_type").getString("title"));
                    Log.e("TAG", dataMap.get("title"));

                    title.setText(dataMap.get("title"));
                    points.setText(dataMap.get("points"));
                    description.setText(dataMap.get("description"));











                    if (pDialog.isShowing())
                        pDialog.dismiss();








                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        hideDialog();

        return null;


    }

    public void startAction(View view){

        Intent intent;
        switch (dataMap.get("type")){
            case "nfc":
                intent = new Intent(getApplicationContext(),NfcActivity.class);
                intent.putExtra("data", dataMap);
                startActivity(intent);
                break;
            case "qr":
                intent = new Intent(getApplicationContext(),QrActivity.class);
                intent.putExtra("data", dataMap);
                startActivity(intent);
                break;
            case "photo":
                intent = new Intent(getApplicationContext(),PhotoActivity.class);
                intent.putExtra("data", dataMap);
                startActivity(intent);
                break;
            case "gps":
                intent = new Intent(getApplicationContext(),GpsActivity.class);
                intent.putExtra("data", dataMap);
                startActivity(intent);
                break;
        }




    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
