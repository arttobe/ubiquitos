package de.web_lichtdesign.stuactalpha.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;
import java.util.UUID;

public class ListActivity extends AppCompatActivity {
    private ProgressDialog pDialog;
    private ListView lv;
    private ArrayList<HashMap<String, String>> activityList;
    private ListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        lv = (ListView) findViewById(R.id.list_activity);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        activityList = new ArrayList<>();
        adapter = new SimpleAdapter(
                ListActivity.this, activityList,
                R.layout.list_activity_item, new String[]{"title", "typ", "points","id"
        }, new int[]{R.id.a_title,R.id.a_typ, R.id.a_points});
        getActivities();
    }


    private Void getActivities(Void... arg0) {
            String tag_string_req = "req_list_activities";
            pDialog.setMessage("Loading");
            showDialog();
           StringRequest strReq = new StringRequest(Request.Method.GET,
                   AppConfig.URL_LIST_ACTIVITIES, new Response.Listener<String>() {

               @Override
               public void onResponse(String response) {



                   try {

                       JSONArray activities = new JSONArray(response);

                       for (int i = 0; i < activities.length(); i++) {
                           JSONObject c = activities.getJSONObject(i);

                           String id = c.getString("id");
                           String title = c.getString("title");
                           String points = "Points: "+c.getString("points");
                           String typ = "Typ: ";



                           // tmp hash map for single contact
                           HashMap<String, String> tmp = new HashMap<>();

                           // adding each child node to HashMap key => value
                           tmp.put("id", id);
                           tmp.put("title", title);
                           tmp.put("typ", typ);
                           tmp.put("points", points);



                           // adding contact to contact list
                           activityList.add(tmp);
                       }
                       if (pDialog.isShowing())
                           pDialog.dismiss();


                       lv.setAdapter(adapter);
                       lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                           @Override
                           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                               Intent intent = new Intent(getApplicationContext(),ShowActivity.class);
                               intent.putExtra("id", activityList.get(position).get("id"));
                               //based on item add info to intent
                               startActivity(intent);

                           }
                       });




                   } catch (JSONException e) {
                       // JSON error
                       e.printStackTrace();
                       Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                   }

               }
           }, new Response.ErrorListener() {

               @Override
               public void onErrorResponse(VolleyError error) {

                   Toast.makeText(getApplicationContext(),
                           error.getMessage(), Toast.LENGTH_LONG).show();

               }
           }) {

               @Override
               protected Map<String, String> getParams() {
                   // Posting parameters to login url
                   Map<String, String> params = new HashMap<String, String>();


                   return params;
               }

           };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
            hideDialog();
            return null;


        }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }






}
