package de.web_lichtdesign.stuactalpha.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;

import java.io.File;
import java.util.HashMap;

import de.web_lichtdesign.stuactalpha.R;
import de.web_lichtdesign.stuactalpha.app.AppConfig;
import de.web_lichtdesign.stuactalpha.app.AppController;
import de.web_lichtdesign.stuactalpha.helper.SQLiteHandler;
import de.web_lichtdesign.stuactalpha.helper.SessionManager;

public class NfcUploadActivity extends AppCompatActivity {
    String img;

    EditText comment;
    String strComment;
    HashMap<String,String> data;
    HashMap<String, String> user;
    private SQLiteHandler db;
    private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_upload);
        Intent i = getIntent();


        data = (HashMap<String, String>)i.getSerializableExtra("data");


        comment = (EditText) findViewById(R.id.comment);

        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());



        // Fetching user details from sqlite
        user = db.getUserDetails();




    }

    public void uploadAction(View view){
        strComment = comment.getText().toString();
        imageUpload(img);
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);

    }
    public void goBackAction(){
        finishActivity(1);
    }

    private void imageUpload(final String imagePath) {

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, AppConfig.URL_DO_ACTIVITY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);

                    }
                }, new  Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

            }


        });


        smr.addMultipartParam("user_id", "text/plain", user.get("id"));
        smr.addMultipartParam("activity_id", "text/plain", data.get("id"));
        smr.addMultipartParam("com", "text/plain", strComment);
        AppController.getInstance().addToRequestQueue(smr);

    }
}
